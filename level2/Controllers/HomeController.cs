﻿using level2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace level2.Controllers
{
    public class HomeController : Controller
    {
        private DB_CodingIDEntities1 db = new DB_CodingIDEntities1();
        public ActionResult Index()
        {
            return View(db.ms_employee.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}