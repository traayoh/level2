﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(level2.Startup))]
namespace level2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
